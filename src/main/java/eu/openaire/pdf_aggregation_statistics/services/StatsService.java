package eu.openaire.pdf_aggregation_statistics.services;


public interface StatsService {

    boolean gatherNumberOfPayloadsPerDatasource(int retryCount);

}

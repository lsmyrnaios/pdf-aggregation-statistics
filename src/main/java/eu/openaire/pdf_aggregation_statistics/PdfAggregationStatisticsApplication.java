package eu.openaire.pdf_aggregation_statistics;

import eu.openaire.pdf_aggregation_statistics.util.UriBuilder;
import jakarta.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PdfAggregationStatisticsApplication {

    private static final Logger logger = LoggerFactory.getLogger(PdfAggregationStatisticsApplication.class);


    private static ConfigurableApplicationContext context;

    public static void main(String[] args) {
        context = SpringApplication.run(PdfAggregationStatisticsApplication.class, args);
    }


    public static void gentleAppShutdown()
    {
        int exitCode = 0;
        try {
            exitCode = SpringApplication.exit(context, () -> 0);    // The "PreDestroy" method will be called. (the "context" will be closed automatically (I checked it))
        } catch (IllegalArgumentException iae) {
            logger.error(iae.getMessage()); // This will say "Context must not be null", in case the "gentleAppShutdown()" was called too early in the app's lifetime. But it's ok.
        }
        System.exit(exitCode);
    }


    @PreDestroy
    public void preDestroy() {
        // TODO - Add any future shut-down logic here (e.g. shutting down threads).
        logger.info("Exiting..");
    }


    @Bean
    public CommandLineRunner setServerBaseUrl(Environment environment, ServletWebServerApplicationContext webServerAppCtxt)
    {
        return args -> new UriBuilder(environment, webServerAppCtxt);
    }

}

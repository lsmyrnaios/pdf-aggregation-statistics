package eu.openaire.pdf_aggregation_statistics.controllers;


import eu.openaire.pdf_aggregation_statistics.services.StatsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;


/**
 * This controller returns statistics for the database.
 */
@RestController
@RequestMapping("/stats")
public class StatsController {

    private static final Logger logger = LoggerFactory.getLogger(StatsController.class);


    // This is a public API, so we will only support statistics that are actually needed and used by certain OpenAIRE Services.
    // So for now, only a few requested metrics will be available.


    // This method shows the parameters which are missing when dealing with the bulk-import API.
    // Spring Boot does not show any specific messages to the user (like stacktraces), for security reasons.
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<?> handleMissingParams(MissingServletRequestParameterException ex) {
        return ResponseEntity.badRequest().body(String.format("Missing parameter: %s\n", ex.getParameterName()));
    }


    /**
     * This endpoint returns the number of payloads related to the given datasourceID.
     * Example of a datasourceID (ArXiv): opendoar____::6f4922f45568161a8cdf4ad2299f6d23
     * */
    @GetMapping(value = "getNumberOfPayloadsForDatasource", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> getNumberOfPayloadsForDatasource(@RequestParam String datasourceId)
    {
        if ( logger.isDebugEnabled() )
            logger.debug("Received a \"getNumberOfPayloadsForDatasource\" request for datasourceID: " + datasourceId);

        String errorMsg = "The given \"datasourceID\": \"" + datasourceId + "\" is not an valid datasourceID.";
        if ( datasourceId.length() != 46 ) {
            logger.error(errorMsg + " The number of its characters is different than 46.");
            return ResponseEntity.badRequest().body(errorMsg);
        } else {
            String[] parts = datasourceId.split("::", 2);   // At most 2 parts will come out of the initial string.
            if ( (parts.length != 2) || (parts[0].length() != 12) || (parts[1].length() != 32) ) {
                logger.error(errorMsg + " It has non-valid parts.");
                return ResponseEntity.badRequest().body(errorMsg);
            }
        }

        // Search the Hashmap and get the value for this datasource.
        // The Map has the numOfPayloads for all datasources, even for newly added ones.
        // If the given datasourceID is not found in the map, then either is not a datasource or that datasource is not participating in the OpenAIRE Graph.

        Integer numPayloads = StatsServiceImpl.datasourcesWithNumOfPayloads.get(datasourceId);
        if ( numPayloads == null )
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The given \"datasourceID\": \"" + datasourceId + "\" does not exist in the OpenAIRE Graph.");
        else
            return ResponseEntity.ok(Integer.toString(numPayloads));
    }

}

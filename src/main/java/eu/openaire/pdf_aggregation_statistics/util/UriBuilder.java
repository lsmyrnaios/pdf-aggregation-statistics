package eu.openaire.pdf_aggregation_statistics.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.core.env.Environment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;


public class UriBuilder {

    private static final Logger logger = LoggerFactory.getLogger(UriBuilder.class);

    public static String ip = null;
    public static String baseUrl = null;

    public UriBuilder(Environment environment, ServletWebServerApplicationContext webServerAppCtxt) {
        baseUrl = "http";

        String sslEnabled = environment.getProperty("server.ssl.enabled");
        if (sslEnabled == null) { // It's expected to not exist if there is no SSL-configuration.
            logger.warn("No property \"server.ssl.enabled\" was found in \"application.yml\". Continuing with plain HTTP..");
            sslEnabled = "false";
        }
        baseUrl += sslEnabled.equals("true") ? "s" : "";
        baseUrl += "://";

        String defined_address = environment.getProperty("server.root_address");
        if ( (defined_address != null) && !defined_address.isEmpty() )
            ip = defined_address;
        else if ( (ip = getPublicIP()) == null )
            ip = InetAddress.getLoopbackAddress().getHostAddress();   // Non-null.

        baseUrl += ip + ":" + webServerAppCtxt.getWebServer().getPort();

        String baseInternalPath = environment.getProperty("server.servlet.context-path");
        if ( baseInternalPath != null ) {
            if ( !baseInternalPath.startsWith("/") )
                baseUrl += "/";
            baseUrl += baseInternalPath;
            if ( !baseInternalPath.endsWith("/") )
                baseUrl += "/";
        } else {
            logger.warn("No property \"server.servlet.context-path\" was found in \"application.yml\"!");    // Yes it's expected.
            baseUrl += "/";
        }

        logger.debug("ServerBaseURL: " + baseUrl);
    }

    private static String getPublicIP()
    {
        String publicIpAddress = "";
        HttpURLConnection conn = null;
        String urlString = "https://checkip.amazonaws.com/";
        try {
            conn = (HttpURLConnection) new URL(urlString).openConnection();
            conn.setConnectTimeout(60_000); // 1 minute
            conn.setReadTimeout(120_000);   // 2 minutes
            conn.setRequestMethod("GET");
            conn.connect();

            int responseCode = conn.getResponseCode();
            if ( responseCode != 200 ) {
                logger.warn("Cannot get the publicIP address for this machine, as \"" + urlString + "\" returned the HTTP-error-code: " + responseCode);
                return null;
            }

            try ( BufferedReader bf = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                publicIpAddress = bf.readLine().trim();
            }
        } catch (Exception e) {
            logger.warn("Cannot get the publicIP address for this machine, from \"" + urlString + "\"!", e);
            return null;
        } finally {
            if ( conn != null )
                conn.disconnect();
        }
        return publicIpAddress;
    }

    public static String getBaseUrl() {
        return baseUrl;
    }

    public static void setBaseUrl(String baseUrl) {
        UriBuilder.baseUrl = baseUrl;
    }

}

package eu.openaire.pdf_aggregation_statistics.components;

import eu.openaire.pdf_aggregation_statistics.PdfAggregationStatisticsApplication;
import eu.openaire.pdf_aggregation_statistics.services.StatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SchedulingTasks {

    private static final Logger logger = LoggerFactory.getLogger(SchedulingTasks.class);


    @Autowired
    StatsService statsService;


    public static boolean runningFirstTime = true;


    @Scheduled(initialDelay = 60_000, fixedDelayString = "${spring.application.cacheUpdateInterval}") // Run 60 seconds after initialization and then every 6 hours.
    //@Scheduled(initialDelay = 45_000, fixedDelay = 300_000) // Just for testing: Run 45 seconds after initialization and then every 5 mins.
    public void gatherPayloadsPerDatasource()
    {
        // Request the number of payloads for each datasource and keep them in a ConcurrentHashMap,
        // where the "key" will be the "datasourceId" and the "value" will be the numOfPayloads for that datasource.

        // When the user requests the numOfPayloads for a given datasourceI, the app will return the result immediately.
        // It will be a quick O(1) GET operation in the ConcurrentHashMap.

        if ( ! statsService.gatherNumberOfPayloadsPerDatasource(0)
            && runningFirstTime )
            PdfAggregationStatisticsApplication.gentleAppShutdown();

        runningFirstTime = false;
    }

}

# PDF Aggregation Statistics [![Build Status](https://jenkins-dnet.d4science.org/buildStatus/icon?job=PDF+Aggregation+Statistics)](https://jenkins-dnet.d4science.org/job/PDF%20Aggregation%20Statistics/)

This is a public API to get specific statistics from the PDF Aggregation Service.<br>
This service is designed to respond as fast as possible, by caching the relevant data in memory, instead of running new queries to the database, for every request.
**The cached data is updated every 6 hours**, by default.<br>
It is possible to configure the "**cacheUpdateInterval**" value inside the [__application.yml__](https://code-repo.d4science.org/lsmyrnaios/pdf-aggregation-statistics/src/branch/master/src/main/resources/application.yml) file.
<br>
<br>

**Statistics API**:
- "**getNumberOfPayloadsForDatasource**" endpoint:  **http://\<IP\>:\<PORT\>/api/stats/getNumberOfPayloadsForDatasource?datasourceId=\<givenDatasourceId\>** <br>
  This endpoint returns the number of payloads which belong to the datasource specified by the given datasourceID.
<br>
<br>

**To install and run the application**:
- Run ```git clone``` and then ```cd pdf_aggregation_statistics```.
- Set the preferable values inside the [__application.yml__](https://code-repo.d4science.org/lsmyrnaios/pdf-aggregation-statistics/src/branch/master/src/main/resources/application.yml) file.
- Execute the ```installAndRun.sh``` script which builds and runs the app.<br>
  If you want to just run the app, then run the script with the argument "1": ```./installAndRun.sh 1```.<br>
  If you want to build and run the app on a **Docker Container**, then run the script with the argument "0" followed by the argument "1": ```./installAndRun.sh 0 1```.<br>
<br>

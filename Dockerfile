FROM eclipse-temurin:17-jre-alpine

COPY build/libs/pdf_aggregation_statistics-*.jar pdf_aggregation_statistics.jar

EXPOSE 1882

ENTRYPOINT ["java","-jar","/pdf_aggregation_statistics.jar", "--spring.config.location=file:///mnt/config/application.yml"]
